const Motor = require('./motor')
const prompt = require("prompt-sync")({ sigint: true });

function main(){
    let motorku = new Motor('Vario', 'Hitam', 'Honda', '2022');
    const speed = prompt('Tentukan Kecepatan motor kamu: ');
    const mundur = prompt('Apakah Motor kamu mundur? (true/false): ');
    const asal = prompt('Tentukan Kota asal Kamu: ');
    const tujuan = prompt('Tentukan Kota tujuan Kamu: ');
    const jarak = prompt('Tentukan perkiraan Jarak: ');
    console.log("========================================Output========================================")
    console.log(`Data Motor: \nModel Motor saya adalah: ${motorku.model} \nWarna Motor saya adalah: ${motorku.warna} \nMerk Motor saya adalah: ${motorku.merk} \nTahun Motor saya adalah: ${motorku.tahun}`);

    motorku.drive(speed);
    motorku.reverse(mundur);
    motorku.tujuan(asal, tujuan, jarak)
    motorku.perkiraan(speed, jarak)
}

main()